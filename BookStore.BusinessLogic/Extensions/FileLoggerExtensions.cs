﻿using BookStore.BusinessLogic.Common;
using Microsoft.Extensions.Logging;
 
namespace BookStore.BusinessLogic.Extensions
{
    public static class FileLoggerExtensions
    {
        public static ILoggerFactory AddFile(this ILoggerFactory factory,
                                        string filePath)
        {
            factory.AddProvider(new FileLoggerProvider(filePath));
            return factory;
        }
    }
}
