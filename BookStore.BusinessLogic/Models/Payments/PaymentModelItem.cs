﻿using BookStore.BusinessLogic.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Payments
{
    public class PaymentModelItem:BaseModel
    {
        public string PrintingEditionName { get; set; }
        public decimal Price { get; set; }
        public decimal Amount { get; set; }
        public int Quantity { get; set; }
    }
}
