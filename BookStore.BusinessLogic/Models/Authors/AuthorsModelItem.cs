﻿using BookStore.BusinessLogic.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Authors
{
    public class AuthorsModelItem : BaseModel
    {
        public long AuthorId { get; set; }
        public string AuthorName { get; set; }
    }
}
