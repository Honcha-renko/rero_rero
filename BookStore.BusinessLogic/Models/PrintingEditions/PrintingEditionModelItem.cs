﻿using BookStore.BusinessLogic.Models.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.PrintingEditions
{
    public class PrintingEditionModelItem:BaseModel
    {
        public string IconUrl { get; set; }
        public string Name { get; set; }
        public string Author { get; set; }
        public decimal PriceMin { get; set; }
        public decimal PriceMax { get; set; }
        public string Sorting { get; set; }
        public decimal Price { get; set; }
        public int CategoryId { get; set; }
        public int CurrencyId { get; set; }
        public int Quantity { get; set; }
        public int PageNumber { get; set; }
    }
}
