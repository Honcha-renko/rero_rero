﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.PrintingEditions
{
    public class PrintingEditionModel
    {
        public List<PrintingEditionModelItem> Items = new List<PrintingEditionModelItem>();
    }
}
