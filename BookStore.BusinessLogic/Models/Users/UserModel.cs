﻿using System.Collections.Generic;
using BookStore.BusinessLogic.Models.Base;
using BookStore.DataAccess.Entities;

namespace BookStore.BusinessLogic.Models.Users
{
    public class UserModel : BaseModel
    {
        public List<UserModelItem> Items = new List<UserModelItem>();
    }
}
