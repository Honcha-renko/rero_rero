﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Account
{
    public class RecoveryPasswordModel
    {
        public string Email { get; set; }
    }
}
