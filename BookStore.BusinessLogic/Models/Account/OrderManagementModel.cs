﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Admin
{
    public class OrderManagementModel
    {
        public long OrderId { get; set; }
        public DateTime Date { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Enums.Type Type { get; set; }
        public string Title { get; set; }
        public int Quantity { get; set; }
        public decimal Amount { get; set; }
        public Enums.Status Status { get; set; }
        public int PageNumber { get; set; }
    }
}