﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Account
{
    public class SignInModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
}
