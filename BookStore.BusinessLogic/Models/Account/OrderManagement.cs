﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Admin
{
    public class OrderManagement
    {
        public long OrderId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Enums.Type Type { get; set; }
        public string Author { get; set; }
        public decimal Price { get; set; }
    }
}
