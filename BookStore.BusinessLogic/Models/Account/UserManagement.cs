﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.BusinessLogic.Models.Admin
{
    public class UserManagement
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Enums.Status Status { get; set; }
    }
}
