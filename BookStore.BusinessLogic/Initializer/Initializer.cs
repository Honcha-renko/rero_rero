﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Initialization;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccess.Repositories.EFRepositories;
using BookStore.BusinessLogic.Services.Interfaces;
using BookStore.BusinessLogic.Services;
using BookStore.DataAccess.Repositories.Base.Interfaces;
using BookStore.DataAccess.Repositories.Base;
using BookStore.BusinessLogic.Models.Users;
using BookStore.BusinessLogic.Helpers;

namespace BookStore.BusinessLogic.Initializer
{
    public class Initializer
    {
        public static void Init(IServiceCollection services, string connection)
        {
            services.AddDbContext<ApplicationContext>(options =>options.UseSqlServer(connection));

            services.AddIdentity<ApplicationUser, Role>()
                .AddEntityFrameworkStores<ApplicationContext>()
                .AddDefaultTokenProviders();

            services.AddTransient<DataBaseInitialization>();

            services.AddTransient<IAuthorRepository, AuthorEFRepository>();
            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<IUserRepository, UserEFRepository>();
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<IEmailHelper, EmailHelper>();
        }
    }
}
