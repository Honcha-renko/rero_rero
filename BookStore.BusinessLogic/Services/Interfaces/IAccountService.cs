﻿using BookStore.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services.Interfaces
{
    public interface IAccountService
    {
        IEnumerable<ApplicationUser> FilterByName(string firstName, string lastName);
        Task<string> SignUpAsync(ApplicationUser user, string password);
        Task<bool> SignInAsync(string userName, string password);
        Task SignOutAsync();
     
    }
}
