﻿using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services.Interfaces
{
    public interface IEmailHelper
    {
        Task SendEmailAsync();
    }
}
