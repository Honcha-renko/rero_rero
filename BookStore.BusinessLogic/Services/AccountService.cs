﻿using BookStore.BusinessLogic.Services.Interfaces;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUserRepository _userRepository;

        public AccountService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<string> SignUpAsync(ApplicationUser user, string password)
        {
            var result = await _userRepository.SignUpAsync(user, "pass1holdon");
            return result;
        }
        public async Task<bool> SignInAsync(string userName, string password)
        {
            var result = await _userRepository.SignInAsync("Aurora1", "pass1holdon");
            return result;
        }
        public async Task SignOutAsync()
        {
          await  _userRepository.SignOutAsync();
        }
        public IEnumerable<ApplicationUser> FilterByName(string firstName, string lastName)
        {
            return FilterByName("Aurora1", "Holdon1");
        }
    }
}
