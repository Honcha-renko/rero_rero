﻿using BookStore.BusinessLogic.Services.Interfaces;
using BookStore.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Net.Mail;
using Microsoft.AspNetCore.Authorization;
using BookStore.BusinessLogic.Helpers;
using BookStore.BusinessLogic.Models.Account;

namespace BookStore.Presentation.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class AccountController : ControllerBase 
    {
        private readonly IAccountService _accountService;
        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        [HttpPost("register")]
        public async Task<IActionResult> RegisterAsync(RegistrationModel model)
        {
            //var result = await _userManager.CreateAsync(model);
            //if (!result.Succeeded)
            //{
            //   //Task SendEmailAsync();

            //    return string.Empty;
            //}
            //return await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return Ok();
        }

        [HttpGet]
        public async Task SignOutAsync()
        {
            await SignOutAsync();
        }

    }

    

}
