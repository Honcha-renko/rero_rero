﻿using BookStore.BusinessLogic.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.Presentation.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _autrhorService;
        public AuthorController(IAuthorService autrhorService)
        {
            _autrhorService = autrhorService;
        }

        [HttpGet("getAll")]
        public async Task<IActionResult> GetAll()
        {
            var result = await _autrhorService.GetAllAsync();
            return Ok("Some Result");
        }
    }
}
