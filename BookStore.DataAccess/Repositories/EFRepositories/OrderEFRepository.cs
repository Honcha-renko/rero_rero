﻿using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class OrderEFRepository : BaseEFRepository<Order>, IOrderRepository
    {
        public OrderEFRepository(ApplicationContext context) : base(context)
        {
        }
        public new async Task<List<Order>> GetAllAsync()
        {
            var result = await base.GetAllAsync();
            return result.ToList();
        }
    }
}
