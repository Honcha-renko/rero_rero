﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class UserEFRepository : IUserRepository
    {
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public UserEFRepository(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public async Task<string> GetRolesAsync(string email)
        {
            var result = await _userManager.FindByEmailAsync(email);
            var roles = await _userManager.GetRolesAsync(result);
            var role = roles.FirstOrDefault();
            return role;
        }
        public async Task<bool> CheckPasswordAsync(ApplicationUser user, string password)
        {
            var result = await _userManager.CheckPasswordAsync(user, password);
            return result;
        }
        public async Task<bool> IsInRoleAsync(ApplicationUser user, string role)
        {
            var result = await _userManager.IsInRoleAsync(user, role);
            return result;
        }
        public IEnumerable<ApplicationUser> FilterByName(string firstName, string lastName)
        {
            return _userManager.Users.Where(x => x.FirstName.Contains(firstName) || x.LastName.Contains(firstName)).AsEnumerable();
        }
        public async Task<string> SignUpAsync(ApplicationUser user, string password)
        {
            var existingUser = await _userManager.FindByEmailAsync(user.Email);
            if (existingUser != null)
            {
                return string.Empty;
            }
            var result = await _userManager.CreateAsync(user, password);
            if (!result.Succeeded)
            {
                return string.Empty;
            }
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<bool> SignInAsync(string userName, string password)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                return false;
            }
            var isUserLocked = await _userManager.IsLockedOutAsync(user);
            if (isUserLocked)
            {
                return false;
            }
            var result = await _signInManager.CheckPasswordSignInAsync(user, password, lockoutOnFailure:false);
            return result.Succeeded;
        }
        public async Task SignOutAsync()
        {
            await _signInManager.SignOutAsync();
        }
        public async Task<IdentityResult> UpdateAsync(ApplicationUser user)
        {
            var result = await _userManager.UpdateAsync(user);
            return result;
        }
        public async Task<bool> DeleteAsync(ApplicationUser user)
        {
            user.IsRemoved = true;
            await _userManager.UpdateAsync(user);
            return user.IsRemoved;
        }
        public async Task<bool> LockoutAsync(ApplicationUser user, DateTimeOffset? lockoutEnd)
        {
            await _userManager.SetLockoutEnabledAsync(user, true);
            var result = await _userManager.SetLockoutEndDateAsync( user, lockoutEnd);
            return result.Succeeded;
        }
        //public async Task<ApplicationUser> GetUserAsync(ClaimsPrincipal principal string userEmail)
        //{
        //   var result= await _userManager.GetUserAsync(userEmail);
        //    return result;
        //}
        public async Task<bool> ResetPasswordAsync(ApplicationUser user, string token, string newPassword)
        {
            var result = await _userManager.ResetPasswordAsync(user, token, newPassword);
            return result.Succeeded;
        }
        
    }
}
