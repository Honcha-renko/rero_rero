﻿using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class AuthorInPrintingEditionEFRepository : BaseEFRepository<AuthorInPrintingEdition>, IAuthorInPrintingEditionRepository
    {
        public AuthorInPrintingEditionEFRepository(ApplicationContext context) : base(context)
        {
        }
        public IEnumerable<AuthorInPrintingEdition> SearchByAuthor(int authorId)
        {
            return _context.AuthorInPrintingEditions.Where(x => x.AuthorId == authorId).AsEnumerable();
        }
        public IEnumerable<AuthorInPrintingEdition> FindByPrintingEdition(int printingEditionId)
        {
            return _context.AuthorInPrintingEditions.Where(y => y.PrintingEditionId == printingEditionId).AsEnumerable();
        }
        public IEnumerable<AuthorInPrintingEdition> FindByAuthor(int authorId)
        {
            return _context.AuthorInPrintingEditions.Where(y => y.AuthorId == authorId).AsEnumerable();
        }

    }
}
