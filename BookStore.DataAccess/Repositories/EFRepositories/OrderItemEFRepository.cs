﻿using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class OrderItemEFRepository: BaseEFRepository<OrderItem>, IOrderItemRepository
    {
         public OrderItemEFRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
