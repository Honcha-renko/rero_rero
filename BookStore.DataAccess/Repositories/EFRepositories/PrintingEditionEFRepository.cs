﻿using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class PrintingEditionEFRepository : BaseEFRepository<PrintingEdition>, IPrintingEditionRepository
    {
        public PrintingEditionEFRepository(ApplicationContext context) : base(context)
        {
        }
        public new async Task<List<PrintingEdition>> GetAllAsync()
        {
            var result = await base.GetAllAsync();
            return result.ToList();
        }
        public IEnumerable<PrintingEdition> FilterForPrintingEdition(decimal pricefilter,string typefilter)
        {
            return _context.PrintingEditions.Where(x => x.Price == pricefilter && x.Type.Equals(typefilter)).AsEnumerable();
        }

        public IEnumerable<PrintingEdition> PriceSort(decimal priceMin, decimal priceMax )
        {
            //var result = PrintingEdition.OrderBy(u => u.Name).ThenBy(u => u.Age);
            return _context.PrintingEditions.Where(x => x.Price >= priceMin && x.Price <= priceMax).AsEnumerable();
            //var sortedUsers = from x in PrintingEdition
            //                  orderby x.Price descending
            //                  select x;
            
        }

    }
}
