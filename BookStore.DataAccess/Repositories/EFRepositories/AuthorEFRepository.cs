﻿using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class AuthorEFRepository : BaseEFRepository<Author>, IAuthorRepository
    {
        public AuthorEFRepository(ApplicationContext context) : base(context)
        {
        }

        public IEnumerable<Author> FilterByName(string firstName, string lastName)
        {
            return _context.Authors.Where(x => x.Name.Contains(firstName)).AsEnumerable();
        }

        public new async Task<List<Author>> GetAllAsync(/*CriteriaFilter*/)
        {
            var result = await base.GetAllAsync();
            return result.ToList();
        }
    }
    
}
