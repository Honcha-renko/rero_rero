﻿using System.Collections.Generic;
using BookStore.DataAccess.Entities.Base;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace BookStore.DataAccess.Repositories.Base.Interfaces
{
    public interface IBaseEFRepository<T> where T : BaseEntity
    {
        Task InsertAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<IEnumerable<T>> GetAllAsync();
    }
}