﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DataAccess.Entities.Base;
using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Repositories.Base.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace BookStore.DataAccess.Repositories.Base
{
    public class BaseEFRepository<T> : IBaseEFRepository<T> where T : BaseEntity
    {
        protected readonly ApplicationContext _context;
        protected readonly DbSet<T> _dbSet;
        public BaseEFRepository(ApplicationContext context)
        {
            _context = context;
            _dbSet = context.Set<T>();
        }
        public async Task InsertAsync(T entity)
        {
            await _context.Set<T>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }
        public async Task UpdateAsync(T entity)
        {
            _context.Set<T>().Update(entity);
            await _context.SaveChangesAsync();
        }
        public Task DeleteAsync(T entity)
        {
            entity.IsRemoved = true;
            _context.Set<T>().Update(entity);
            return _context.SaveChangesAsync();
        }

        public T Get(int id)
        {
            return _dbSet.Find(id);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Set<T>().ToListAsync();
        }
    }
}