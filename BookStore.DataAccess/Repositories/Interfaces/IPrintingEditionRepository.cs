﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IPrintingEditionRepository : IBaseEFRepository<PrintingEdition>
    {
        new Task<List<PrintingEdition>> GetAllAsync();
        IEnumerable<PrintingEdition> FilterForPrintingEdition(decimal pricefilter, string typefilter);
        IEnumerable<PrintingEdition> PriceSort(decimal priceMin, decimal priceMax);
    }
}
