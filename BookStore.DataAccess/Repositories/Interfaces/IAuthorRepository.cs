﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base.Interfaces;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IAuthorRepository : IBaseEFRepository<Author>
    {
        IEnumerable<Author> FilterByName(string firstName, string lastName);
       // Task<Author> InsertAsync(string firstName);
       // Task<Author> GetByFirstName(string firstName);
    }
}
