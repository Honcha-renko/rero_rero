﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base.Interfaces;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IOrderRepository : IBaseEFRepository<Order>
    {
    }
}
