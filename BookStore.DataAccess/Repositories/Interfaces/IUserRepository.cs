﻿using BookStore.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IUserRepository
    {
        // Task<string> GetUserAsync(ApplicationUser user);
        Task<bool> LockoutAsync(ApplicationUser user, DateTimeOffset? lockoutEnd);
        Task<string> GetRolesAsync(string email);
        Task<string> SignUpAsync(ApplicationUser user, string password);
        Task<bool> SignInAsync(string userName, string password);
        Task SignOutAsync();
        IEnumerable<ApplicationUser> FilterByName(string firstName, string lastName);
        Task<bool> CheckPasswordAsync(ApplicationUser user, string password);
        Task<bool> IsInRoleAsync(ApplicationUser user, string role);
        Task<IdentityResult> UpdateAsync(ApplicationUser user);
        Task<bool> DeleteAsync(ApplicationUser user);
        Task<IdentityResult> ResetPasswordAsync(ApplicationUser user, string token, string newPassword);
    }

}
