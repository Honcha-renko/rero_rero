﻿using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Base.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IAuthorInPrintingEditionRepository: IBaseEFRepository <AuthorInPrintingEdition>
    {
        IEnumerable<AuthorInPrintingEdition> FindByAuthor(int authorId);
        IEnumerable<AuthorInPrintingEdition> FindByPrintingEdition(int printingEditionId);
    }
}
