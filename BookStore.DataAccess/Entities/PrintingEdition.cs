﻿using BookStore.DataAccess.Entities.Base;

namespace BookStore.DataAccess.Entities
{
    public class PrintingEdition : BaseEntity 
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int Price { get; set; }
        public Enums.Type Type { get; set; }
        public Enums.Status Status{ get; set; }
        public Enums.Currency Currency { get; set; }
    }
}
