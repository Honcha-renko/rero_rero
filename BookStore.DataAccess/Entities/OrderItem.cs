﻿using BookStore.DataAccess.Entities.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccess.Entities
{
    public class OrderItem : BaseEntity
    {
        public decimal Amount { get; set; }
        public int Count { get; set; }
        public Enums.Currency Currency { get; set; }

        public long PrintingEditionId { get; set; }
        [ForeignKey("PrintingEditionId")]
        public PrintingEdition PrintingEdition { get; set; }

        public long OrderId { get; set; }
        [ForeignKey("OrderId")]
        public Order Order { get; set; }
    }
}
