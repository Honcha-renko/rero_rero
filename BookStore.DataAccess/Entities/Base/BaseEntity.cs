﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccess.Entities.Base
{
    public class BaseEntity
    {
        public long Id { get; set; }

        public DateTime CreateDate {get; set;}
        public bool IsRemoved { get; set; }
    }
}
