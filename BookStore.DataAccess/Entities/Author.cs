﻿using BookStore.DataAccess.Entities.Base;

namespace BookStore.DataAccess.Entities
{
    public class Author: BaseEntity
    {
        public string Name { get; set; }
    }
}
