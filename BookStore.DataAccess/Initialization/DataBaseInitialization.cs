﻿using BookStore.DataAccess.AppContext;
using BookStore.DataAccess.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;

namespace BookStore.DataAccess.Initialization
{
    public class DataBaseInitialization
    {
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<Role> _roleManager;
        private readonly ApplicationContext _context;

        public DataBaseInitialization(ApplicationContext context)
        {
            _context = context;
        }

        public void SeedData(UserManager<ApplicationUser> userManager, RoleManager<Role> roleManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;

            SeedRoles();
            SeedUsers();
            SeedEntities();
        }

        public void SeedEntities()
        {
            _context.Database.EnsureCreated();

            if (_context.Authors.Any())
            {
                return;
            }

            var author = new Author
            {
                Name = "Author 1"
            };
            _context.Set<Author>().AddAsync(author).GetAwaiter().GetResult();

            var printingeditions = new PrintingEdition
            {
                Title = "Title 1",
                Description = "Description1",
                Price = 45,
                Status =Enums.Status.Unpaid,
                Currency = Enums.Currency.USD,
                Type =Enums.Type.Book
            };
            _context.Set<PrintingEdition>().AddAsync(printingeditions).GetAwaiter().GetResult();

            var authorsInPrintingEditions = new AuthorInPrintingEdition
            {
                AuthorId = author.Id,
                PrintingEditionId = printingeditions.Id
            };
            _context.Set<AuthorInPrintingEdition>().AddAsync(authorsInPrintingEditions).GetAwaiter().GetResult();
            _context.SaveChangesAsync().GetAwaiter().GetResult();
            _context.Dispose();
        }

        public void SeedUsers()
        {
            if (_userManager.FindByEmailAsync("Email@1").GetAwaiter().GetResult() != null)
            {
                return;
            }

            var admin = new ApplicationUser
            {
                FirstName = "Aurora1",
                LastName = "Holdon1",
                PasswordHash = "PasswordHash1",
                Email = "Email@1",
                UserName = "Admin",
                EmailConfirmed = true,
                IsRemoved = false,
                LockoutEnabled = false,
                CreationDate = DateTime.Now
            };
            var result = _userManager.CreateAsync(admin).GetAwaiter().GetResult();

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(admin, "Admin").Wait();
            }
            if (_userManager.FindByEmailAsync("Email@2").GetAwaiter().GetResult() != null)
            {
                return;
            }
            var user = new ApplicationUser
            {
                
                FirstName = "Red2",
                LastName = "Wingdings2",
                PasswordHash = "PasswordHash2",
                Email = "Email@2",
                UserName = "User",
                IsRemoved = false,
                LockoutEnabled = true,
                CreationDate = DateTime.Now
            };
            result = _userManager.CreateAsync(user).GetAwaiter().GetResult();

            if (result.Succeeded)
            {
                _userManager.AddToRoleAsync(user, "User").Wait();
            }  
        }
        
        public  void SeedRoles()
        {
            if (!_roleManager.RoleExistsAsync("Admin").Result)
            {
                var role = new Role
                {
                    Name = "Admin"
                };
                var roleResult = _roleManager.CreateAsync(role).Result;
            }
            if (!_roleManager.RoleExistsAsync("User").Result)
            {
                var role = new Role
                {
                    Name = "User"
                };
                var roleResult = _roleManager.CreateAsync(role).Result;
            }
        }
    }
}